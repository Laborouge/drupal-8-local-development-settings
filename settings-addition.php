<?php
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

$settings['config_sync_directory'] = 'profiles/imagospirit/config/sync';

$config['config_split.config_split.localhost']['status'] = TRUE;

$settings['file_private_path'] = 'sites/default/files/private';
$settings['file_temp_path'] = 'sites/default/files/tmp';

$settings['trusted_host_patterns'] = [
  '^monsite\.local$',
  '^.+\.monsite\.local$',
];
