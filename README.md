# Drupal 8 / 9 local development settings

Custom files for local development in Drupal 8 / 9

This project contains :
- [settings.local.php](settings.local.php)
- [development.services.yml](development.services.yml)
- [settings-addition.php](additionnal entries for settings.php)

These files must be present in the "/sites/default" folder of your Drupal installation.
